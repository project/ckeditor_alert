This module will create a CKEditor button to easily add alert message using CKEditor.

Adds a new button to Drupal's CKEditor which allows the user to create & display any type of content in an alert box format.

The styling is minimal and easily over writeable by developers.