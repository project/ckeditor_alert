/*
* Alert Box Plugin
*
* @author Sagar Boina <sagar.sijju@gmail.com>
* @version 1.0
*/
(function () {
    CKEDITOR.plugins.add( 'alert', {
        icons: 'alert',
        init: function( editor ) {
            editor.addCommand( 'alert', new CKEDITOR.dialogCommand( 'alert' ) );
            editor.ui.addButton( 'alert', {
                label: 'Add Alert Message',
                command: 'alert',
                toolbar: 'insert'
     
            });
            let plugin_path = this.path;
			
			
			CKEDITOR.dialog.add('alert', function (editor) {
				return {
					title: 'Alert Message',
					minWidth: 400,
					minHeight: 200,
					contents: [
						{
							id: 'AlertPlugin',
							label: 'Basic Settings',
							elements: [
								{
									type: 'textarea',
									id: 'alert_text',
									label: 'Alert Message',
									setup: function( element ) {
										this.setValue( element.getText() );
									},
									commit: function( element ) {
										element.setText( this.getValue() );
									}
								}
							]
						}
					],
					onShow: function() {
						var selection = editor.getSelection();
						var element = selection.getStartElement();
						this.element = element;
						this.setupContent( this.element );						
					},
					onOk: function() {
						var txt = this.getValueOf('AlertPlugin', 'alert_text');
						var img_src = plugin_path + 'icons/alert.png';
						var list_txt = '<span class="ck-alert-box"><span class="ck-alert-important"><img src="'+img_src+'"></span><span class="ck-txt"><span class="ck-red">Important! </span>'+txt+'</span></span>';
						editor.insertHtml( list_txt );
					}
				}
			}); //dialog ends
    } //init ends
	}) //add ends
})()